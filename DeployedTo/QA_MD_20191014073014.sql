-------------------------------------------
-- 20191011_1420_US16533_MD.sql
-- created by L.Dukes
-- created on 10/11/2019
-------------------------------------------
SET ECHO ON
SET FEEDBACK ON
SET DEFINE OFF

WHENEVER SQLERROR EXIT ROLLBACK 

SELECT systimestamp FROM dual;

ALTER SESSION SET current_schema = MEMDBOWN;

create index mdb_member_access_token_idx1 on memdbown.mdb_member_access_token (access_token) parallel 4 online;
alter index memdbown.mdb_member_access_token_idx1 noparallel;

create index mdb_member_access_token_idx2 on memdbown.mdb_member_access_token (refresh_token) parallel 4 online;
alter index memdbown.mdb_member_access_token_idx2 noparallel;

BEGIN
   dbms_stats.gather_table_stats( ownname => 'MEMDBOWN'
                                 ,tabname => 'mdb_member_access_token'
                                 ,estimate_percent => dbms_stats.auto_sample_size
                                 ,CASCADE => TRUE
                                 ,method_opt => 'FOR ALL INDEXED COLUMNS SIZE AUTO'
                                 ,degree => 12
                                );
END;
/

SELECT systimestamp FROM dual;
-- ----------------------------------
-- End of script
-- ----------------------------------

